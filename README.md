# Worm Gene Phenotypes
### Compatible with Ensembl WBcel235 Release 81

##### Usage:
```
# Install package
devtools::install_bitbucket('jeevb/WB249.Phenotypes.WBcel235.81')

# Load library
library(WB249.Phenotypes.WBcel235.81)

# Source dataset
data(phenotypes.list)
```

Data was sourced from WormBase Release 249 as follows:
```
GetPhenotypes <- function(wb.id) {
    require(rvest)

    baseURL <- 'http://www.wormbase.org/rest/widget/gene/%s/phenotype'
    tryCatch({
        phenotypes <-
            read_html(sprintf(baseURL, wb.id)) %>%
            html_nodes(xpath='//table[@id="table_phenotype_observed_by"]//a[@class="phenotype-link"]') %>%
            html_text() %>%
            unique() %>%
            tolower()
        data.frame(wb.id=wb.id, phenotype=phenotypes)
    }, error=function(err) {
        NULL
    })
}


##
## Main
##

require(staR)
require(plyr)

gtfdb <- ParseGTF('~/genomes/Caenorhabditis_elegans.WBcel235.81/Caenorhabditis_elegans.WBcel235.81.gtf')
proteins <- GeneIDs(GenesByType(gtfdb, 'protein_coding'))

# Get phenotypes
phenotypes <- ldply(proteins, GetPhenotypes)
phenotypes.list <- with(phenotypes, split(wb.id, phenotype))
```
